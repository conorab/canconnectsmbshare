# CanConnectSMBShare

A script and SystemD unit used to check if a given SMB share can be contacted so dependent SMB services are not started unless the share is available. This was made in 2017 and is no longer maintained.
