#!/bin/bash

# CanConnectSMBShare - (Check if Possible to Connect to S.M.B. Share) - v1.0

# Exit Codes

	# 1 - The first argument (the SystemD mount unit to obtain information from) is null.
	# 2 - One or more of the cells in the 'ValidOptionMappings' variable contains less than 2 sub-cells.
	# 3 - Non-zero exit code returned while obtaining the path of the SystemD unit given in the first argument.
	# 4 - Non-zero exit code returned while reading the SystemD unit file.
	# 5 - Could not find a line in the SystemD unit file starting with 'Options='.
	# 6 - Grep returned an exit code of 2 (error) while trying to find lines starting with 'Options=' in the SystemD unit file.
	# 7 - Grep returned an unknown exit code while trying to find lines starting with 'Options=' in the SystemD unit file.
	# 8 - Cut returned a non-zero exit code while trying to trim the line starting with 'Options=' in the SystemD unit file.
	# 9 - The options line in the SystemD unit file does not contain any comma-delimited options.
	# 10 - The SystemD unit file contains more than one line starting with 'Options='.
	# 11 - Non-zero exit code returned by systemctl while getting the 'What' option for the unit given in the first argument.
	# 12 - Non-zero exit code returned by 'StripStartString' while trimming the 'What' option from the SystemD unit given in the first argument.

function StripStartString

	{
		if echo -n "$2" | cut -c "$((${#1}+1))"\- 2> /dev/null

			then
				return 0

			else
				return 1
		fi
	}

if [ -z "$1" ]

	then
		echo 'CanConnectSMBShare: The first argument (the SystemD mount unit to obtain information from) is null.' >&2
		exit 1
fi

unset ValidOptionMappings
ValidOptionMappings[0]='credentials,authentication-file'
ValidOptionMappings[1]='username,username'
ValidOptionMappings[2]='password,password'
ValidOptionMappings[3]='port,port'

# Make sure that each cell in the 'ValidOptionMappings' variable contains at least 2 sub-cells.

	unset ValidOptionMappings_ProcCount
	ValidOptionMappings_ProcCount=\0

	while [ "$ValidOptionMappings_ProcCount" -lt "${#ValidOptionMappings[*]}" ]

		do
			unset ValidOptionMappings_Current
			set -f
			IFS=\,
			ValidOptionMappings_Current=(${ValidOptionMappings[$ValidOptionMappings_ProcCount]})

			if [ "${#ValidOptionMappings_Current[*]}" -lt \2 ]

				then
					echo 'CanConnectSMBShare: Cell '"$(($ValidOptionMappings_ProcCount+1))"' in the '\''ValidOptionMappings'\'' variable contains less than 2 sub-cells.' >&2
					exit 2
			fi

			((ValidOptionMappings_ProcCount++))
	done

	unset ValidOptionMappings_ProcCount
	unset ValidOptionMappings_Current

# Get the 'What' option from the SystemD unit.
# This is the mount source.

	unset MountSource_Dirty
	if ! MountSource_Dirty="$(systemctl -p What show "$1" 2> /dev/null)"

		then
			echo 'CanConnectSMBShare: Non-zero exit code returned by '\''systemctl'\'' while getting the '\''What'\'' option from the '\'"$1"\'' unit.' >&2
			exit 11
	fi

	unset MountSource
	if ! MountSource="$(StripStartString 'What=' "$MountSource_Dirty" 2> /dev/null)"

		then
			echo 'CanConnectSMBShare: Non-zero exit code returned by '\''StripStartString'\'' while trimming the '\''What'\'' option from the SystemD unit '\'"$1"\'\. >&2
			exit 12
	fi

	unset MountSource_Dirty

# Get the path of the SystemD unit file given in the first argument.
# We do this because there doesn't seem to be a trival way to obtain the contents of the 'Options' argument of the SystemD unit via systemctl, so we're using Grep to do this instead, which requires the path.

	unset SystemDUnitPath
	if ! SystemDUnitPath="$(systemctl -p FragmentPath show "$1" 2> /dev/null)"

		then
			echo 'CanConnectSMBShare: Non-zero exit code returned while getting the path of the following unit: '\'"$1"\'\. >&2
			exit 3
	fi

	unset MountUnitPath
	MountUnitPath="$(StripStartString 'FragmentPath=' "$SystemDUnitPath")"

# Load the contents of the SystemD unit file into R.A.M.
# We could have Grep read the file directly and pipe that into cut, but we would need to enable errors for pipe failures, and that still wouldn't tell us what in that pipeline went wrong.
# Because of this, we are doing each step in the pipeline seperately so we can check the exit code of each program in the pipeline and output different, more meaningful errors.

	unset SystemDUnitContents
	if ! SystemDUnitContents="$(<"$MountUnitPath")"

		then
			echo 'CanConnectSMBShare: Non-zero exit code returned while trying to read the SystemD unit '\'"$1"\'' located at '\'"$MountUnitPath"\'\. >&2
			exit 4
	fi

# Find the line in the SystemD unit file startinbg with 'Options='.
# Because this script assumes that there is only one line like this later on, we check that this is the case at a later state and fail.
# See the section above where we load the SystemD unit file into memory for why this isn't a single pipeline.

	unset SystemDUnitContents_OptionsLineOnly
	unset SystemDUnitContents_OptionsLineOnly_ExitCode
	SystemDUnitContents_OptionsLineOnly="$(echo -n "$SystemDUnitContents" | grep '^Options=')"
	SystemDUnitContents_OptionsLineOnly_ExitCode="$?"
	case "$SystemDUnitContents_OptionsLineOnly_ExitCode" in

		0)
			;;

		1)
			echo 'CanConnectSMBShare: Could not find a line in the SystemD unit file (located at '\'"$MountUnitPath"\'') starting with '\''Options='\'\. >&2
			exit 5
			;;

		2)
			echo 'CanConnectSMBShare: Grep returned an exit code of 2 (error) when trying to obtain only lines starting with '\''Options='\'' in the SystemD unit file (located at ('\'"$MountUnitPath"\'').' >&2
			exit 6
			;;

		*)
			echo 'CanConnectSMBShare: Grep returned an unknown exit code ('\'"$SystemDUnitContents_OptionsLineOnly_ExitCode"\'') while trying to find lines starting with '\''Options=' in the SystemD unit file located at '\'"$MountUnitPath"\'\. >&2
			exit 7
	esac

	unset SystemDUnitContents_OptionsLineOnly_ExitCode
	unset SystemDUnitContents

# Make sure that there is only one line in the SystemD unit file starting with 'Options='.
# The rest of this script assumes this and will 'silently' screw up if this is not the case.

	IFS=$'\n'
	unset SystemDUnitContents_OptionsLineOnly_Array
	set -f
	SystemDUnitContents_OptionsLineOnly_Array=($SystemDUnitContents_OptionsLineOnly)
	if [ "${#SystemDUnitContents_OptionsLineOnly_Array[*]}" -gt \1 ]

		then
			echo 'CanConnectSMBShare: The SystemD unit ('\'"$MountUnitPath"\'') file contain more than one line starting with '\''Options='\'\. >&2
			exit 10
	fi
	unset SystemDUnitContents_OptionsLineOnly_Array

# Make a comma-delimited array of the 'Options=' variable from the SystemD unit file.
# In making this array, we only want the data after 'Options=' at the start of the line, so we 'cut' everything before that out.
# See the section above where we load the SystemD unit file into memory for why this isn't a single pipeline.

	IFS=\,
	if ! Options=($(echo -n "$SystemDUnitContents_OptionsLineOnly" | cut -c 9- 2> /dev/null))

		then
			echo 'CanConnectSMBShare: Cut returned a non-zero exit code while trimming the line in the SystemD unit file (located at '\'"$MountUnitPath"\'') starting with '\''Options='\'\. >&2
			exit 8
	fi

# Make sure the 'Options' argument from the SystemD unit file contains at least one option.

	if [ "${#Options[*]}" -le \0 ]

		then
			echo 'CanConnectSMBShare: The Options line in the SystemD unit file ('\'"$MountUnitPath"\'') does not contain any comma-delimited options.' >&2
			exit 9
	fi

# Iterate over each option in the Options array and see if that option name matches something in the 'ValidOptionMappings' array.
# Each cell in the 'ValidOptionMappings' contains at least 2 comma-delimited sub-cells/values.
# All but the last value in each (non-sub) cell is checked against to see if the option we're currently checking in the Options array matches.
# All but the last value of each sub-cell are qualifies tied to the last value in the sub-cell.
# If the current option name ('credentials' for example) matches any of the values except for the last in a sub-cell, then we substitude the option name in the Options array for the lasy comma-seperated value in the sub-cell ('credentials' becomes 'authentication-file').
# This is because mount.cifs has some different option names for the same option in smbclient, and so to pass that option to smbclient, it may need to be substituted.
# The substitute option name may be the same as the option name in the Options array.
# Even though no substitution needs to occur to pass that mount.cifs value to smbclient, it allows as to only pass options from the Options array that we actually need/know how to handle to smbclient.
# Options in the Options array with no counterparts in the 'ValidOptionMappings' array are ignored and not passed onto smbclient.
# If an option in the Options array repeats, the last one will override the one before it.
# All of this is done to build a string of arguments which we send to smbclient (gets appended to the ClientOptions array).
# This is why the first option is '-c exit'.

	unset Options_ProcCount
	Options_ProcCount=\0

	unset ClientOptions

	ClientOptions[0]='-c exit'

	IFS=\,
	for Options_Current in ${Options[*]}

		do
			unset ValidOptionMappings_ProcCount
			ValidOptionMappings_ProcCount=\0

			while [ "$ValidOptionMappings_ProcCount" -lt "${#ValidOptionMappings[*]}" ]

				do
					unset ValidOptionMappings_Current
					IFS=\,
					ValidOptionMappings_Current=(${ValidOptionMappings[$ValidOptionMappings_ProcCount]})

					unset ValidOptionMappings_Current_ProcCount
					ValidOptionMappings_Current_ProcCount=\0

					unset ValidOptionMappings_Current_Matched
					ValidOptionMappings_Current_Matched=\0

					while [ "$ValidOptionMappings_Current_ProcCount" -lt "$((${#ValidOptionMappings_Current[*]}-1))" ] && [ "$ValidOptionMappings_Current_Matched" == \0 ]

						do
							unset ValidOptionMappings_Current_Current
							ValidOptionMappings_Current_Current="${ValidOptionMappings_Current[$ValidOptionMappings_Current_ProcCount]}"

							if [ "$(echo -n "$Options_Current" | cut -c \-$((${#ValidOptionMappings_Current_Current}+1)))" == "$ValidOptionMappings_Current_Current"\= ]

								then
									ClientOptions[${#ClientOptions[*]}]='--'"${ValidOptionMappings_Current[$((${#ValidOptionMappings_Current[*]}-1))]}"\="$(StripStartString "$ValidOptionMappings_Current_Current"\= "$Options_Current")"

									ValidOptionMappings_Current_Matched=\1
							fi

							((ValidOptionMappings_Current_ProcCount++))
					done

					((ValidOptionMappings_ProcCount++))
			done
	done

IFS=' '
smbclient ${ClientOptions[*]} "$MountSource"
